{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Custom.Theme.Tokyonight (
    dimColor,
    backgroundColor,
    foregroundColor,
    selectionBackground,
    selectionForeground,
    dimSelectionForeground,
    brightSelectionForeground,
    superBrightSelectionForeground,
    color0,
    color1,
    color2,
    color3,
    color4,
    color5,
    color6,
    color7,
    color8,
    color9,
    color10,
    color11,
    color12,
    color13,
    color14,
    color15,
    color16,
    color17,
) where

dimColor = "#444444"

-- Main 16
backgroundColor = color0
foregroundColor = color15
selectionBackground = "#2D4F67"
selectionForeground = color7
dimSelectionForeground = color8
brightSelectionForeground = color16
superBrightSelectionForeground = color9

-- Normal
color0 = "#24283b"
color1 = "#1f2335"
color2 = "#292e42"
color3 = "#565f89"
color4 = "#a9b1d6"
color5 = "#c0caf5"
color6 = "#c0caf5"
color7 = "#c0caf5"

-- Bright
color8 = "#f7768e"
color9 = "#ff9e64"
color10 = "#e0af68"
color11 = "#9ece6a"
color12 = "#1abc9c"
color13 = "#41a6b5"
color14 = "#bb9af7"
color15 = "#ff007c"

-- extended colors
color16 = "#b6927b"
color17 = "#b98d7b"
