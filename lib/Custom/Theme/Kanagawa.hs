{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Custom.Theme.Kanagawa (
    dimColor,
    backgroundColor,
    foregroundColor,
    selectionBackground,
    selectionForeground,
    dimSelectionForeground,
    brightSelectionForeground,
    superBrightSelectionForeground,
    color0,
    color1,
    color2,
    color3,
    color4,
    color5,
    color6,
    color7,
    color8,
    color9,
    color10,
    color11,
    color12,
    color13,
    color14,
    color15,
    color16,
    color17,
) where

dimColor = "#444444"

-- Main 16
backgroundColor = color0
foregroundColor = color15
selectionBackground = "#2D4F67"
selectionForeground = color7
dimSelectionForeground = color8
brightSelectionForeground = color16
superBrightSelectionForeground = color9

-- normal
color0 = "#16161D"
color1 = "#C34043"
color2 = "#76946A"
color3 = "#C0A36E"
color4 = "#7E9CD8"
color5 = "#957FB8"
color6 = "#6A9589"
color7 = "#C8C093"

-- bright
color8 = "#727169"
color9 = "#E82424"
color10 = "#98BB6C"
color11 = "#E6C384"
color12 = "#7FB4CA"
color13 = "#938AA9"
color14 = "#7AA89F"
color15 = "#DCD7BA"

-- extended colors
color16 = "#b6927b"
color17 = "#b98d7b"
