{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Custom.Theme.Setting (
    myBorderWidth,
    myNormalBorderColor,
    myFocusedBorderColor,
    backgroundColor,
    foregroundColor,
    selectionBackground,
    selectionForeground,
    dimSelectionForeground,
    brightSelectionForeground,
    color0,
    color1,
    color2,
    color3,
    color4,
    color5,
    color6,
    color7,
    color8,
    color9,
    color10,
    color11,
    color12,
    color13,
    color14,
    color15,
    color16,
    color17,
    myDefPP,
    xmobarCommand,
    myDefEMConf,
) where

import XMonad (Dimension, def)
import XMonad.Actions.EasyMotion (EasyMotionConfig (..))
import XMonad.Hooks.DynamicLog (PP (..), shorten, wrap, xmobarColor)

-- import Custom.Theme.Ayu
-- import Custom.Theme.Kanagawa
-- import Custom.Theme.Tokyonight
-- import Custom.Theme.KanagawaDragon
import Custom.Theme.RosepineDawn

myDefEMConf :: EasyMotionConfig
myDefEMConf =
    def
        { emFont = "xft: FiraCode-100"
        , bgCol = backgroundColor
        , txtCol = foregroundColor
        , borderCol = color4
        , borderPx = 10
        }

xmobarCommand :: String -> String
xmobarCommand name =
    concat
        [ "xmobar "
        , " $HOME/.config/xmobar/"
        , name
        , " -B" <> backgroundColor
        , " -F" <> dimSelectionForeground
        ]

myDefPP :: PP
myDefPP =
    def
        { ppCurrent = xmobarColorBox superBrightSelectionForeground
        , ppHidden = xmobarColorBox dimSelectionForeground
        , ppHiddenNoWindows = xmobarColorBox dimColor
        , ppVisible = xmobarColorWrapBox brightSelectionForeground "*" ""
        , ppLayout = xmobarColorBox dimSelectionForeground
        , ppTitle = xmobarColorBox dimSelectionForeground . shorten 35
        , ppSep = "<fc=" <> dimColor <> "> <> </fc>"
        }
  where
    xmobarColorBox c = xmobarColorWrapBox c "" ""
    xmobarColorWrapBox c wl wr = xmobarColor c "" . wrap wl wr . boxWrap c
    boxWrap x = wrap ("<box type=Bottom width=1 mb=1 color=" <> x <> ">") "</box>"

-- Border
myBorderWidth = 3 :: Dimension
myNormalBorderColor = color0
myFocusedBorderColor = color5 -- color4
