{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Custom.Theme.KanagawaDragon (
    dimColor,
    backgroundColor,
    foregroundColor,
    selectionBackground,
    selectionForeground,
    dimSelectionForeground,
    brightSelectionForeground,
    superBrightSelectionForeground,
    color0,
    color1,
    color2,
    color3,
    color4,
    color5,
    color6,
    color7,
    color8,
    color9,
    color10,
    color11,
    color12,
    color13,
    color14,
    color15,
    color16,
    color17,
) where

dimColor = "#444444"

-- Main 16
backgroundColor = color0
foregroundColor = color15
selectionBackground = "#2D4F67"
selectionForeground = color7
dimSelectionForeground = color8
brightSelectionForeground = color16
superBrightSelectionForeground = color9

-- normal
color0 = "#0f0f18" -- "#0d0c0c"
color1 = "#c4746e"
color2 = "#8a9a7b"
color3 = "#c4b28a"
color4 = "#8ba4b0"
color5 = "#a292a3"
color6 = "#8ea4a2"
color7 = "#c8c093"

-- bright
color8 = "#a6a69c"
color9 = "#e46876"
color10 = "#87a987"
color11 = "#e6c384"
color12 = "#7fb4ca"
color13 = "#938aa9"
color14 = "#7aa89f"
color15 = "#c5c9c5"

-- extended colors
color16 = "#b6927b"
color17 = "#b98d7b"
