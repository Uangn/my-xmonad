{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Custom.Theme.Ayu (
    dimColor,
    backgroundColor,
    foregroundColor,
    selectionBackground,
    selectionForeground,
    dimSelectionForeground,
    brightSelectionForeground,
    superBrightSelectionForeground,
    color0,
    color1,
    color2,
    color3,
    color4,
    color5,
    color6,
    color7,
    color8,
    color9,
    color10,
    color11,
    color12,
    color13,
    color14,
    color15,
    color16,
    color17,
) where

dimColor = "#444444"

-- Main 16
backgroundColor = color0
foregroundColor = color15
selectionBackground = "#253340"
selectionForeground = color7
dimSelectionForeground = color4
brightSelectionForeground = color15
superBrightSelectionForeground = color17

-- normal
color0 = "#0f1419"
color1 = "#131721"
color2 = "#272d38"
color3 = "#3e4b59"
color4 = "#bfbdb6"
color5 = "#e6e1cf"
color6 = "#e6e1cf"
color7 = "#f3f4f5"

-- bright
color8 = "#f07178"
color9 = "#ff8f40"
color10 = "#ffb454"
color11 = "#b8cc52"
color12 = "#95e6cb"
color13 = "#59c2ff"
color14 = "#d2a6ff"
color15 = "#e6b673"

-- extended colors
color16 = "#f07178"
color17 = "#e05178"
