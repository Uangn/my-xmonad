{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Custom.Theme.RosepineDawn (
    dimColor,
    backgroundColor,
    foregroundColor,
    selectionBackground,
    selectionForeground,
    dimSelectionForeground,
    brightSelectionForeground,
    superBrightSelectionForeground,
    color0,
    color1,
    color2,
    color3,
    color4,
    color5,
    color6,
    color7,
    color8,
    color9,
    color10,
    color11,
    color12,
    color13,
    color14,
    color15,
    color16,
    color17,
) where

dimColor = "#444444"

-- Main 16
backgroundColor = color0
foregroundColor = color15
selectionBackground = "#575279"
selectionForeground = color7
dimSelectionForeground = color8
brightSelectionForeground = color16
superBrightSelectionForeground = color9

-- Normal
color0 = "#faf4ed"
color1 = "#fffaf3"
color2 = "#f2e9de"
color3 = "#9893a5"
color4 = "#797593"
color5 = "#575279"
color6 = "#575279"
color7 = "#cecacd"

-- Bright
color8 = "#b4637a"
color9 = "#ea9d34"
color10 = "#d7827e"
color11 = "#286983"
color12 = "#56949f"
color13 = "#907aa9"
color14 = "#ea9d34"
color15 = "#cecacd"

-- extended colors
color16 = "#b6927b"
color17 = "#b98d7b"
