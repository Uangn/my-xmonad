{-# LANGUAGE FlexibleContexts #-}

module Custom.Util (
    debugLog,
    swapToToggleWS,
    swapToToggleWS',
    lastViewedHiddenExcept,
    tryMoveTo,
    mfindWorkspace,
    mapWorkspaces,
    setToggleActiveAll,
    setToggleActive,
    batteryPresent,
) where

import Control.Monad (void, when)
import Data.Foldable
import System.Directory (doesFileExist)
import System.IO (IOMode (..), hPutStrLn, withFile)
import XMonad
import XMonad.Actions.CycleWS
import qualified XMonad.Hooks.WorkspaceHistory as WH
import XMonad.Layout.MultiToggle (Toggle (Toggle), Transformer (..), isToggleActive)
import qualified XMonad.StackSet as W

debugLog :: X ()
debugLog =
    withWindowSet
        ( \ws -> do
            liftIO $ print ws
            liftIO $ logToTmpFile $ show ws
        )

myAppendFile :: FilePath -> String -> IO ()
myAppendFile f s = do
    withFile f AppendMode $ \h -> do
        hPutStrLn h s

logToTmpFile :: String -> IO ()
logToTmpFile = myAppendFile "/tmp/xmonad.log" . (++ "\n")

swapToToggleWS :: X ()
swapToToggleWS = swapToToggleWS' []

swapToToggleWS' :: [WorkspaceId] -> X ()
swapToToggleWS' skips = lastViewedHiddenExcept skips >>= flip whenJust (windows . W.shift)

lastViewedHiddenExcept :: [WorkspaceId] -> X (Maybe WorkspaceId)
lastViewedHiddenExcept skips = do
    hs <- gets $ map W.tag . flip skipTags skips . W.hidden . windowset
    choose hs . find (`elem` hs) <$> WH.workspaceHistory
  where
    choose [] _ = Nothing
    choose (h : _) Nothing = Just h
    choose _ vh@(Just _) = vh

tryMoveTo :: [String] -> ManageHook
tryMoveTo [] = def
tryMoveTo (w : ws) =
    mfindWorkspace w >>= maybe (tryMoveTo ws) doShift

mfindWorkspace :: String -> Query (Maybe WorkspaceId)
mfindWorkspace w =
    liftX (withWindowSet $ pure . find (w ==) . fmap W.tag . W.workspaces)

mapWorkspaces :: (WindowSpace -> X b) -> X [b]
mapWorkspaces f = withWindowSet $ traverse f . W.workspaces

setToggleActiveAll :: (Transformer t Window) => t -> Bool -> X ()
setToggleActiveAll t a = void $ mapWorkspaces (setToggleActive t a)

setToggleActive :: (Transformer t Window) => t -> Bool -> WindowSpace -> X ()
setToggleActive t b ws = do
    g <- fmap (if b then not else id) <$> isToggleActive t ws
    maybe (pure ()) (`when` sendMessageWithNoRefresh (Toggle t) ws) g

batteryPresent :: IO Bool
batteryPresent = do
    let battFile = "/sys/class/power_supply/BAT0/present"
    doesFileExist battFile
