{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Custom.Layout (
    toggleFloat,
    myLayout,
    layoutDefaults,
    MyToggles (..),
) where

import qualified Data.Map as M
import Graphics.X11 (Window)
import XMonad
import XMonad.Core (LayoutClass (description, doLayout, emptyLayout), Typeable, X)
import XMonad.Hooks.ManageDocks (avoidStruts)
import XMonad.Layout (Tall (Tall), (|||))
import XMonad.Layout.AvoidFloats (AvoidFloatMsg (..), avoidFloats)
import XMonad.Layout.Decoration
import XMonad.Layout.LayoutBuilder
import XMonad.Layout.LayoutModifier (ModifiedLayout (..))
import XMonad.Layout.MagicFocus (magicFocus)
import XMonad.Layout.Magnifier (magnifiercz)
import XMonad.Layout.MultiToggle (Toggle (..), Transformer (..), mkToggle1)
import XMonad.Layout.MultiToggle.Instances (StdTransformers (MIRROR, NBFULL, NOBORDERS, SMARTBORDERS))
import XMonad.Layout.NoBorders (Ambiguity (..), lessBorders)
import XMonad.Layout.Renamed (Rename (..), renamed)
import XMonad.Layout.SimplestFloat (simplestFloat)
import XMonad.Layout.Spacing (smartSpacing)
import XMonad.Layout.Tabbed (simpleTabbed, tabbed, tabbedAlways)
import XMonad.Layout.ThreeColumns (ThreeCol (ThreeColMid))
import XMonad.Layout.TwoPanePersistent (TwoPanePersistent (..))
import XMonad.Operations (windows)
import qualified XMonad.StackSet as W

import Custom.Theme.Setting

unmodifyLayout :: ModifiedLayout m l a -> l a
unmodifyLayout (ModifiedLayout _ l) = l
unmodifyLayoutTwice :: ModifiedLayout m (ModifiedLayout m1 l) a -> l a
unmodifyLayoutTwice = unmodifyLayout . unmodifyLayout

toggleFloat :: Window -> X ()
toggleFloat w =
    windows
        ( \s ->
            if M.member w (W.floating s)
                then W.sink w s
                else W.float w (W.RationalRect (1 / 3) (1 / 4) (1 / 2) (4 / 5)) s
        )

data MyToggles
    = GAPS
    | MAGICFOCUS
    | MAGNIFY
    | AVOIDSTRUTS
    | EMPTY
    deriving (Read, Show, Eq, Typeable)

prependName :: String -> l a -> ModifiedLayout Rename l a
prependName s = renamed [Prepend (s <> " ")]

instance Transformer MyToggles Window where
    transform GAPS x k = k (smartSpacing 5 x) unmodifyLayout
    transform MAGICFOCUS x k = k (prependName "MagicFocus" $ magicFocus x) unmodifyLayoutTwice
    transform MAGNIFY x k = k (magnifiercz 1.3 x) unmodifyLayout
    transform AVOIDSTRUTS x k = k (avoidStruts x) unmodifyLayout
    transform EMPTY x k = k EmptyLayout (const x)

data EmptyLayout a = EmptyLayout deriving (Show, Read)
instance LayoutClass EmptyLayout a where
    doLayout a b _ = emptyLayout a b
    description _ = "Empty"

layoutDefaults :: X ()
layoutDefaults = do
    sendMessage $ Toggle AVOIDSTRUTS

-- sendMessage $ AvoidFloatSet True

myLayout =
    lessBorders OnlyFloat
        . avoidFloats
        . mkToggle1 AVOIDSTRUTS
        . mkToggle1 EMPTY
        . mkToggle1 NBFULL
        . mkToggle1 NOBORDERS
        . mkToggle1 SMARTBORDERS
        . mkToggle1 GAPS
        . mkToggle1 MIRROR
        . mkToggle1 MAGICFOCUS
        . mkToggle1 MAGNIFY
        $ defaultLay
  where
    defaultLay =
        tallLay
            ||| simpleTabbed
            ||| twoPane
            ||| simplestFloat
            ||| threeCol

tallLay = Tall 1 (3 / 100) (1 / 2)
threeCol = ThreeColMid 1 (3 / 100) (1 / 2)
twoPane = TwoPanePersistent Nothing (3 / 100) (1 / 2)
