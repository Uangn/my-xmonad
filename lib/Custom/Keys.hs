{-# LANGUAGE TupleSections #-}

module Custom.Keys (
    myKeys,
    myModMask,
    modm1,
    modm2,
    modm3,
    modm4,
) where

import Control.Applicative (asum)
import Control.Monad ((>=>))
import Data.Bifunctor (first)
import Data.Foldable
import Data.List (sortBy, sortOn)
import qualified Data.Map as M
import Data.Ord (comparing)
import Data.Ratio ((%))
import System.Exit (exitSuccess)
import Text.Read (readMaybe)
import XMonad
import XMonad.Actions.CycleWS (Direction1D (..), hiddenWS, moveTo, toggleWS)
import XMonad.Actions.DynamicWorkspaceOrder (withNthWorkspace)
import XMonad.Actions.DynamicWorkspaces (removeEmptyWorkspace)
import XMonad.Actions.FocusNth (swapNth)
import XMonad.Actions.Submap (subName, submap, visualSubmapSorted)
import XMonad.Actions.Warp (Corner (..), banish, warpToWindow)
import XMonad.Layout.AvoidFloats
import XMonad.Layout.MultiToggle (Toggle (..))
import XMonad.Layout.MultiToggle.Instances (StdTransformers (MIRROR, NBFULL, NOBORDERS, SMARTBORDERS))
import qualified XMonad.StackSet as W
import XMonad.Util.Paste (sendKey)
import XMonad.Util.XUtils (winBg, winFg)

import Custom.Layout
import Custom.Prompt
import Custom.Theme.Setting
import Custom.Util

myModMask = modm1

modm1 = mod4Mask
modm2 = mod1Mask
modm3 = controlMask
modm4 = shiftMask

myWorkspaceKeys = [xK_a, xK_s, xK_d, xK_f, xK_g, xK_z, xK_x, xK_c, xK_v, xK_b] <> backupWorkspaceKeys
backupWorkspaceKeys = [xK_1 .. xK_9] <> [xK_0]

rofiSettings =
    "rofi \
    \-monitor 1 \
    \-modes 'drun,run,window' "

myKeys conf@(XConfig{XMonad.modMask = modm}) =
    M.fromList $
        ((modm, xK_slash), smap qBinds)
            : fmap rmName mQBinds
                <> kBinds
  where
    mQBinds = fmap (first . first $ (modm .|.)) qBinds

    qBinds =
        concat
            [ qLaunching
            , qWorkspaces
            , qWindows
            , qLayouts
            , qExtra
            , qScreenSwitch
            , sWorkspaceSwitch 0 backupWorkspaceKeys
            , sWorkspaceSwitch 0 myWorkspaceKeys
            , sWorkspaceMove modm2 backupWorkspaceKeys
            , sWorkspaceMove modm2 myWorkspaceKeys
            ]

    kBinds =
        [
            ( (modm, xK_w)
            , ssmap sortFirstInt $ xWorkspaceSwitch 0 <> (rmMod <$> workspaceSelections)
            )
        , kLaunching 0 xK_u
        , kWindows 0 xK_i
        , kLayouts 0 xK_o
        ]
            <> (rmName <$> workspaceSelections)

    workspaceSelections =
        [ ((modm .|. modm2, xK_w), ("Move to Workspace", ssmap sortFirstInt $ xWorkspaceMove 0))
        , ((modm .|. modm3, xK_e), ("Edit Workspaces", ssmap sortFirstInt xWorkspaces))
        ]

    smap = ssmap id
    ssmap s =
        visualSubmapSorted
            s
            def{winBg = backgroundColor, winFg = foregroundColor}
            . M.fromList
    rmMod = (first . first) (const 0)
    rmName (k, (_, a)) = (k, a)
    skipPastChar c = tail . dropWhile (/= c)
    untilNull f = takeWhile (not . null) . iterate f
    maybeFirstInt = asum . fmap (readMaybe :: String -> Maybe Int)

    -- sortSnd = sortBy $ comparing snd
    sortFirstInt = sortOn (maybeFirstInt . words . snd)

    -- Launching
    qLaunching =
        [ ((0, xK_Return), subName "Spawn Terminal" $ spawn $ XMonad.terminal conf)
        , ((0, xK_p), subName "Spawn Launcher" $ spawn $ rofiSettings <> "-show run")
        , ((modm2, xK_p), subName "Spawn Window Picker" $ spawn $ rofiSettings <> "-show window")
        ]

    kLaunching e x =
        ( (modm .|. e, x)
        , smap
            [ ((0, xK_f), subName "Find App" $ spawn $ rofiSettings <> "-show run")
            , -- [ ((0, xK_m), spawn $ rofiSettings<>"rofi -show combi -combi-modes \"drun,run\" -modes combi")
              ((0, xK_w), subName "Search Window" $ spawn $ rofiSettings <> "-show window")
            ]
        )

    -- Workspaces
    qWorkspaces =
        [ ((modm3, xK_j), subName "Workspace Focus Next" $ moveTo Next hiddenWS)
        , ((modm3, xK_k), subName "Workspace Focus Prev" $ moveTo Prev hiddenWS)
        , ((0, xK_n), subName "Workspace Focus Last Viewed" toggleWS)
        , ((modm2, xK_n), subName "Workspace Move to Last Viewed" swapToToggleWS)
        {- , ((modm .|. modm4, xK_j), shiftToNext)
        , ((modm .|. modm4, xK_k), shiftToPrev) -}
        ]

    xWorkspaces =
        [ ((0, xK_s), subName "Select Workspace" $ selectWorkspaceAppend myWSPrompt)
        , ((0, xK_a), subName "Add Workspace" $ myAddWorkspacePrompt myWSPrompt)
        , ((modm, xK_BackSpace), subName "Remove Workspace" removeEmptyWorkspace)
        , ((0, xK_r), subName "Rename Workspace" $ myRenameWorkspace myWSPrompt)
        ]

    onK s f = zipWith (\(m, k) n -> ((m, k), (s <> show n, f n)))
    onKWithWorkspace mk s f ks = onK s (withNthWorkspace f) ((mk .|. 0,) <$> ks) [0 ..]

    xWorkspaceSwitch mk = sWorkspaceSwitch mk myWorkspaceKeys
    sWorkspaceSwitch mk = onKWithWorkspace mk "Find Workspace " W.greedyView

    xWorkspaceMove mk = onKWithWorkspace mk "Move to Workspace " W.shift myWorkspaceKeys
    sWorkspaceMove mk = onKWithWorkspace mk "Move to Workspace " W.shift

    qScreenSwitch =
        onK "Screen Focus " (screenWorkspace >=> flip whenJust (windows . W.view)) ((0,) <$> [xK_e, xK_r, xK_t]) [0 ..]

    -- Windows
    qWindows =
        [ -- Focus
          ((0, xK_j), subName "Window Focus Next" $ windows W.focusDown)
        , ((modm2, xK_j), subName "Window Move to Next" $ windows W.swapDown)
        , ((0, xK_k), subName "Window Focus Prev" $ windows W.focusUp)
        , ((modm2, xK_k), subName "Window Move to Prev" $ windows W.swapUp)
        , -- Master
          ((0, xK_m), subName "Window Focus Master" $ windows W.focusMaster)
        , ((modm2, xK_m), subName "Window Move to Master" $ windows W.swapMaster)
        , -- Size
          ((0, xK_h), subName "Window Shrink" $ sendMessage Shrink)
        , ((0, xK_l), subName "Window Expand" $ sendMessage Expand)
        , -- Effects
          ((modm4, xK_q), subName "Window Kill" kill)
        ]
            <> xSelection

    xSelection =
        [ -- Window Selection
          ((0, xK_semicolon), subName "Window Select" $ mySelectWindow >>= (`whenJust` windows . W.focusWindow))
        ,
            ( (0, xK_apostrophe)
            , subName "Window Move" $ do
                win <- mySelectWindow
                stack <- gets $ W.index . windowset
                let match = find ((win ==) . Just . fst) $ zip stack [0 ..]
                whenJust match $ swapNth . snd
            )
        ]

    kWindows e x =
        ( (modm .|. e, x)
        , smap $
            [ -- Window Handling
              ((modm, xK_q), subName "Quit App" kill)
            , ((0, xK_comma), subName "Increase # of Master" $ sendMessage (IncMasterN 1))
            , ((0, xK_period), subName "Decrease # of Master" $ sendMessage (IncMasterN (-1)))
            ]
                <> xSelection
        )

    -- Layouts
    qLayouts =
        [ -- Rotate through the available layout algorithms
          ((0, xK_space), subName "Layout Rotate" $ sendMessage NextLayout)
        , --  Reset the layouts on the current workspace to default
          ((modm2, xK_space), subName "Layout Reset" $ setLayout (XMonad.layoutHook conf) >> layoutDefaults)
        ]

    kLayouts e x =
        ( (modm .|. e, x)
        , smap
            [ -- Resize viewed windows to the correct size
              ((modm, xK_r), subName "Refresh Windows" refresh)
            , -- Toggles
              ---- FullScreen
              ((0, xK_f), subName "Fullscreen" $ sendMessage $ Toggle NBFULL)
            , ((0, xK_b), subName "Toggle Bar" $ sendMessage $ Toggle AVOIDSTRUTS)
            , ((0, xK_d), subName "Fullscreen + Toggle Bar" $ sendMessage (Toggle AVOIDSTRUTS) >> sendMessage (Toggle NBFULL))
            , ((0, xK_F11), subName "Fullscreen + Toggle Bar + F11" $ sendMessage (Toggle AVOIDSTRUTS) >> sendMessage (Toggle NBFULL) >> sendKey 0 xK_F11)
            , ---- Focusing
              ((0, xK_m), subName "MagicFocus" $ sendMessage $ Toggle MAGICFOCUS)
            , ((0, xK_z), subName "Zoom" $ sendMessage $ Toggle MAGNIFY)
            , ((0, xK_e), subName "Empty" $ sendMessage $ Toggle EMPTY)
            , ---- Layout Modifiers
              ((0, xK_r), subName "Rotate" $ sendMessage $ Toggle MIRROR)
            , ((0, xK_t), subName "Toggle Float" $ withFocused toggleFloat)
            , -- Move Cursor Out Da Wae
              ((0, xK_c), subName "Center Cursor" $ warpToWindow (1 % 2) (1 % 2))
            , ((modm, xK_c), subName "Send Cursor Bottom Right" $ banish LowerRight)
            , -- Notifications
              ((0, xK_n), subName "toggle dunst" $ spawn "dunstctl set-paused toggle")
            , -- Effects
              ((0, xK_a), subName "Toggle Avoid Floats" $ sendMessage AvoidFloatToggle)
            , ((modm, xK_a), subName "Toggle Avoid This Float" $ withFocused $ sendMessage . AvoidFloatToggleItem)
            ]
        )

    -- Extra
    qExtra =
        [ ((0, xK_equal), subName "Zoom In" $ spawn "preztool")
        , -- Lock
          ((modm4, xK_Escape), subName "XMonad Lock" $ spawn "xset s activate")
        , -- Quit xmonad
          ((modm2 .|. modm3, xK_q), subName "XMonad Logout" $ io exitSuccess)
        , -- Restart xmonad
          ((0, xK_q), subName "XMonad Reload Config" $ spawn "killall xmobar; xmonad --recompile; xmonad --restart")
        , -- Run xmessage with a summary of the default keybindings (useful for beginners)

            ( (modm4, xK_slash)
            , subName "XMonad Help" $
                spawn $
                    concat
                        [ runShellCommand <> " '"
                        , "echo \"" <> help <> "\""
                        , " | cat - " <> customDir <> "/Keys.hs " <> customDir <> "/Mouse.hs"
                        , " | less'"
                        ]
            )
        , -- Debug Log
          ((modm4, xK_d), subName "XMonad Dump Debug" debugLog)
        ]

runShellCommand = "alacritty -e sh -c"

customDir = "$HOME/.config/xmonad/lib/Custom"

help :: String
help =
    unlines
        [ "-- Keys"
        , "modm = Super Key"
        , "modm2 = Alt Key"
        , "modm3 = Ctrl Key"
        , ""
        , "-- Open"
        , "modm-Enter       Terminal"
        , "modm-p           Rofi - app picker"
        , ""
        , "-- Movement"
        , "modm-j           Move to Next"
        , "modm-k           Move to Prev"
        , "modm-modm2-j     Shift to Next"
        , "modm-modm2-k     Shift to Prev"
        , ""
        , "read in ~/.xmonad/xmonad.hs"
        ]
