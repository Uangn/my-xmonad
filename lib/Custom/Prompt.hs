module Custom.Prompt (
    myWSPrompt,
    mySelectWindow,
    myRenameWorkspace,
    selectWorkspaceAppend,
    namedWorkspacePrompt,
    myAddWorkspacePrompt,
    myAddWorkspace,
) where

import Data.Ratio ((%))
import XMonad
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.EasyMotion
import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch (fuzzyMatch)
import XMonad.Prompt.Workspace
import qualified XMonad.StackSet as W

import Custom.Layout
import Custom.Theme.Setting

-- Easy Motions
emConf =
    myDefEMConf
        { sKeys = AnyKeys [xK_j, xK_k, xK_h, xK_l]
        , overlayF = proportional 0.7
        , cancelKey = xK_Escape
        }

mySelectWindow = selectWindow emConf

--------------------------------------------------------------------------------
-- Workspace Prompt
myWSPrompt =
    def
        { font = "xft:firacode-20:style=semibold"
        , bgColor = backgroundColor
        , fgColor = foregroundColor
        , bgHLight = selectionBackground
        , fgHLight = selectionForeground
        , complCaseSensitivity = CaseInSensitive
        , searchPredicate = fuzzyMatch
        , height = 40
        , position = CenteredAt{xpWidth = 4 % 10, xpCenterY = 1 % 2}
        -- , alwaysHighlight = True
        -- , promptKeymap = vimLikeXPKeymap' (setBorderColor "grey22") id id isSpace
        }

myRenameWorkspace c = mkXPrompt (Wor "Rename Workspace: ") c (const $ return []) renameWorkspaceByName

selectWorkspaceAppend :: XPConfig -> X ()
selectWorkspaceAppend conf =
    workspacePrompt conf $ \w -> do
        s <- gets windowset
        if W.tagMember w s
            then windows $ W.greedyView w
            else myAddWorkspace w

namedWorkspacePrompt name conf = mkXPrompt (Wor name) conf (const (return []))

-- Workspace Addition
myAddWorkspacePrompt conf = namedWorkspacePrompt "New workspace name: " conf myAddWorkspace
myAddWorkspace w = addWorkspace w >> layoutDefaults
