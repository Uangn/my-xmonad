-- Language
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
-- Lint
{-# HLINT ignore "Redundant $" #-}
{-# HLINT ignore "Eta reduce" #-}
{-# HLINT ignore "Redundant $" #-}
{-# HLINT ignore "Eta reduce" #-}
{-# HLINT ignore "Redundant $" #-}
{-# HLINT ignore "Eta reduce" #-}
{-# OPTIONS_GHC -Wno-missing-signatures #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

-- Haskell Base
-- import Data.Monoid ()
import Data.Bifunctor (Bifunctor (..))
import qualified Data.Map as M
import Data.Ratio ((%))

-- System

-- Main XMonad import
import XMonad
import XMonad.Prelude

-- Utils
import XMonad.Hooks.ManageHelpers (doFullFloat, isFullscreen)
import XMonad.Operations (unGrab)
import XMonad.StackSet (tagMember)
import XMonad.Util.ActionQueue (enqueue)
import XMonad.Util.Paste (sendKey)
import XMonad.Util.SpawnOnce (spawnOnce)

import XMonad.Util.Hacks (fixSteamFlicker, javaHack, trayAbovePanelEventHook, trayPaddingXmobarEventHook, windowedFullscreenFixEventHook)

-- Hooks
import XMonad.Hooks.UrgencyHook (doAskUrgent)
import qualified XMonad.Hooks.WorkspaceHistory as WH

-- Prompts
import XMonad.Hooks.DynamicLog (PP (..))
import XMonad.Prompt (ComplCaseSensitivity (..), XPConfig (..), XPPosition (..), mkXPrompt, setBorderColor, vimLikeXPKeymap')
import XMonad.Prompt.FuzzyMatch (fuzzyMatch)
import XMonad.Prompt.Workspace (Wor (Wor), workspacePrompt)

-- Keybinds
import XMonad.Util.EZConfig (additionalKeysP)

-- Output to other programs
-- Status Bar
import XMonad.Hooks.ManageDocks (AvoidStruts (..), Direction2D (..), avoidStruts, avoidStrutsOn, docks)
import XMonad.Hooks.StatusBar (statusBarProp, withSB)

-- Screens
-- Screen formats
import XMonad.Layout.BinarySpacePartition (FocusParent (..), ResizeDirectional (..), Rotate (..), SelectMoveNode (..), Swap (..), TreeBalance (..), emptyBSP)
import XMonad.Layout.IndependentScreens (countScreens, onCurrentScreen, withScreens, workspaces')
import XMonad.Layout.NoBorders (Ambiguity (..), With (..), lessBorders, noBorders, smartBorders)
import XMonad.Layout.Reflect (reflectHoriz)
import XMonad.Layout.SimplestFloat (simplestFloat)
import XMonad.Layout.Spacing (smartSpacing, smartSpacingWithEdge)
import XMonad.Layout.Tabbed (Theme (..), shrinkText, tabbed)
import XMonad.Layout.ThreeColumns (ThreeCol (ThreeCol, ThreeColMid))
import qualified XMonad.StackSet as W

-- Screen format selectors
import XMonad.Layout.MagicFocus (magicFocus)
import XMonad.Layout.Magnifier (magnifiercz)
import XMonad.Layout.MultiToggle (EOT (..), Toggle (Toggle), Transformer (..), isToggleActive, mkToggle, mkToggle1, (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers (..))

-- Screen format mods
import XMonad.Layout.Renamed (Rename (Prepend), renamed)

-- Window handling
import XMonad.Actions.CycleWS
import XMonad.Actions.EasyMotion (ChordKeys (..), EasyMotionConfig (..), proportional, selectWindow)
import XMonad.Actions.FocusNth (swapNth)
import XMonad.Actions.OnScreen (greedyViewOnScreen, viewOnScreen)
import XMonad.Actions.UpdatePointer (updatePointer)
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen, setEwmhActivateHook)

-- Cursor handling
import XMonad.Actions.Warp (Corner (LowerRight), banish, warpToWindow)

-- Workspaces
import XMonad.Actions.DynamicWorkspaces (addHiddenWorkspace, addWorkspace, addWorkspacePrompt, appendWorkspace, appendWorkspacePrompt, removeEmptyWorkspace, removeEmptyWorkspaceByTag, removeWorkspace, renameWorkspaceByName, selectWorkspace, withNthWorkspace, withWorkspace)
import XMonad.Actions.WorkspaceNames (renameWorkspace)
import XMonad.Layout.LayoutModifier (ModifiedLayout (ModifiedLayout))

-- Theme
import Custom.Keys
import Custom.Layout
import Custom.Mouse
import Custom.Prompt
import Custom.Theme.Setting
import Custom.Util

----------------------------------------

myTerminal = "alacritty"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

myWorkspaces = ["home", "work", "search", "dev", "temp", "music", "ent", "config", "social", "media"]

----------------------------------------

screenshotMain = show "/home/$USER/Pictures/screenshots/"

-- screenshotName = "%Y-%m-%d_%H:%M:%S.png"

------------------------------------------------------------------------
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook =
    composeAll $ defaults <> apps
  where
    defaults =
        [ isFullscreen --> (doF W.focusDown <+> doFullFloat)
        ]
    apps =
        [ className =? "OpenTabletDriver.UX" --> doFloat
        , -- , className =? "Gimp"                --> doFloat
          className =? "xdg-desktop-portal-gtk" --> doFloat
        , className =? "steam" --> tryMoveTo ["ent"]
        , resource =? "desktop_window" --> doIgnore
        , resource =? "kdesktop" --> doIgnore >> tryMoveTo ["games", "media"]
        , className =? "discord" --> tryMoveTo ["social"]
        , className =? "vesktop" --> tryMoveTo ["social"]
        , className =? "VencordDesktop" --> tryMoveTo ["social"]
        , className =? "Spotify" --> tryMoveTo ["music"]
        , className =? "obs" --> tryMoveTo ["rec", "media"]
        , className =? "krita" --> tryMoveTo ["draw", "media"]
        ]

------------------------------------------------------------------------
-- Event handling
myEventHook =
    mconcat
        [ trayAbovePanelEventHook (className =? tray) (appName =? bar)
        , trayPaddingXmobarEventHook (className =? tray) "_XMONAD_TRAYPAD"
        , windowedFullscreenFixEventHook
        , fixSteamFlicker
        ]
  where
    tray = "stalonetray"
    bar = "xmobar"

------------------------------------------------------------------------
-- Status bars and logging
myLogHook = return ()

------------------------------------------------------------------------
-- Startup hook
myStartupHook = do
    setToggleActiveAll AVOIDSTRUTS True
    updatePointer (0.5, 0.5) (1, 1) -- Center cursor on screen
    traverse_
        spawnOnce
        [ "mkdir -p " <> screenshotMain -- Make screenshot folder
        , "source ~/.xprofile"
        ]

compose :: (Foldable t) => t (c -> c) -> c -> c
compose = foldl' (.) id

--------------------------------------------------------------------------------

myPP =
    myDefPP
        { ppOrder = \(ws : l : wt : rs) -> ws : l : wt : rs
        }

main = do
    nScreens <- countScreens
    batt <- batteryPresent
    let sb n name =
            statusBarProp
                (concat [xmobarCommand name, " -x ", show n])
                (pure myDefPP)
        sbs =
            zipWith sb [0 ..] $
                (if batt then map ("battery-" <>) else id)
                    ["m0-xmobarrc", "m1-xmobarrc"]

    xmonad
        $ compose (withSB <$> take (fromIntegral nScreens) sbs)
            . javaHack
            . setEwmhActivateHook doAskUrgent
            . ewmh
            -- . ewmhFullscreen
            . docks
        $ def
            { -- simple stuff
              terminal = myTerminal
            , focusFollowsMouse = myFocusFollowsMouse
            , clickJustFocuses = myClickJustFocuses
            , borderWidth = myBorderWidth
            , modMask = myModMask
            , workspaces = myWorkspaces
            , normalBorderColor = myNormalBorderColor
            , focusedBorderColor = myFocusedBorderColor
            , -- key bindings
              keys = myKeys
            , mouseBindings = myMouseBindings
            , -- hooks, layouts
              layoutHook = myLayout
            , manageHook = myManageHook
            , handleEventHook = myEventHook
            , logHook = myLogHook
            , startupHook = myStartupHook
            }
            -- Audio
            `additionalKeysP` [ ("<XF86AudioLowerVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
                              , ("<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
                              , ("C-<XF86AudioLowerVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ -1%")
                              , ("C-<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ +1%")
                              , ("<XF86AudioMute>", spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
                              , ("<XF86AudioMicMute>", spawn "$HOME/.local/bin/scripts/mute-microphone.sh")
                              , ("M-\\", spawn "$HOME/.local/bin/scripts/mute-microphone.sh")
                              ]
            -- Brightness
            `additionalKeysP` [ ("<XF86MonBrightnessUp>", spawn "brightnessctl set +1%")
                              , ("<XF86MonBrightnessDown>", spawn "brightnessctl --min-value=1% set 1%-")
                              ]
            -- Screenshots
            `additionalKeysP` [ ("<Print>", unGrab *> spawn ("flameshot full --path " <> screenshotMain))
                              , ("S-<Print>", unGrab *> spawn ("flameshot gui --path " <> screenshotMain))
                              , -- Clipboard Screenshots"
                                ("C-<Print>", unGrab *> spawn "flameshot full --clipboard")
                              , ("C-S-<Print>", unGrab *> spawn "flameshot gui --clipboard")
                              ]
